'''

--- EN CONSTRUCTION ---

'''


import sqlite3


class Database():
    def __init__(self, dbfile):
        self.dbfile = dbfile
        self.conn = None
        self.c = None

    def connect_to_db(self):
        self.conn = sqlite3.connect(self.dbfile)
        self.c = self.conn.cursor()

    def disconnect_from_db(self):
        self.conn.commit()
        self.conn.close()


class Team():
    def __init__(self, name):
        self.name = name

    def display_player(self):
        for p in self.players:
            print(p)


class DBDAO():
    def __init__(self, db):
        self.db = db

    def execute(self, req):
        cur = self.db.c
        cur.execute(req)
        return cur


class TeamDAO(DBDAO):
    def create_table_teams(self):
        self.execute("CREATE TABLE teams (name)")

    def insert_team(self, name):
        self.execute("INSERT INTO teams VALUES ?", name)

    def remove_team(self, name):
        self.execute("DELETE FROM teams WHERE name = ?", name)

    def get_all_teams(self):
        teams = []
        try:
            cur = self.execute("SELECT * FROM teams")
            for r in cur:
                teams.append(Team(r[0]))
        except sqlite3.OperationalError:
            print("La table \'teams\' n\'existe pas.")
        return teams


basedonnees = Database('results.db')

team1 = Team('equipe1')
team1.name = 'nomdelequipe'

daodb = DBDAO(basedonnees)
daoteams = TeamDAO(basedonnees)
basedonnees.connect_to_db()
# daoteams.create_table_teams()
print(daoteams.get_all_teams())
basedonnees.disconnect_from_db()
